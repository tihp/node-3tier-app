# Postgres Backup application

This application is deployed as Cronjob workload type. 

By default, the job is triggered once a day at midnight UTC. To change this behaviour, modify the ```deployment.yaml``` file.

```$xslt
apiVersion: batch/v1beta1
kind: CronJob
...
spec:
  concurrencyPolicy: Allow
  schedule: 0 0 * * *
```

By default, a Persistent Volume Claim of 1Gi is created. To update this value, modify the modify the ```deployment.yaml``` file.

```$xslt
apiVersion: v1
kind: PersistentVolumeClaim
...
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
```

This application is meant to be deployed along side with Bitnami postgresql-ha helm chart 
([link](https://github.com/bitnami/charts/tree/master/bitnami/postgresql-ha)). To make it compatible with non-bitnami 
chart, update the ```secretKeyRef``` value for the ```PGPASSWORD``` secret.

```$xslt
apiVersion: apps/v1
kind: Deployment
...
    spec:
      containers:
        - name: postgres-backup-browser
          ...
          env:
            - name: PGPASSWORD
              valueFrom:
                secretKeyRef:
                  key: postgresql-password
                  name: postgresql-postgresql-ha-postgresql
```