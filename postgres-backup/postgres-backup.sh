#!/bin/bash
set -xe

BK=backup_$(date +'%Y%m%d_%T.%N%Z')
: ${RETENTION_DAY:=7}

pg_dump -h ${POSTGRESQL_POSTGRESQL_HA_PGPOOL_SERVICE_HOST:-postgresql-postgresql-ha-pgpool} \
        -p ${POSTGRESQL_POSTGRESQL_HA_PGPOOL_SERVICE_PORT_POSTGRESQL:-5432} \
        -U postgres -d postgres -f "$BK"

gzip "$BK"
find /tmp -name "*.gz" -type f -mtime +"$RETENTION_DAY" -exec rm -f {} \;

