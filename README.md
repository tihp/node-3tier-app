# Sample 3tier App

## Introduction
This repo contains code for a Node.js multi-tier application.

The application overview is as follows:

```$xslt
web <=> api <=> db
```

Each directory contains details of their respective components.

## Deployment

The `api`, `web`, and `postgres-backup` applications can be deployed with Rancher's CICD Pipelines feature 
([link](https://rancher.com/docs/rancher/v2.x/en/k8s-in-rancher/pipelines/)).

For the `terraform` codes, read the `README.md` of each section for the deployment instructions. 