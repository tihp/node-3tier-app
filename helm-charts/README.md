# Helm Charts

## Introduction

This directory contains the Helm charts for [elasticsearch](https://github.com/elastic/helm-charts), 
[kibana](https://github.com/elastic/helm-charts/tree/master/kibana), and 
[postgresql-ha](https://github.com/bitnami/charts/tree/master/bitnami/postgresql-ha).

## Requirements

Helm version 3 or newer is required ([helm installation link](https://helm.sh/docs/intro/install/)).

## Installation

### Elasticsearch

To `install` elasticsearch with `helm`, ensure that the namespace `logging` exists:

```$xslt
kubectl create ns logging
```

Update the `elasticsearch/values-production.yaml` file appropriately and then run:

```$xslt
helm -n logging install elasticsearch elasticsearch -f elasticsearch/values-production.yaml 
```

### Kibana

To `install` kibana with `helm`, ensure that elasticsearch is installed and running. 
Create the namespace `logging`:

```$xslt
kubectl create ns logging
```

Update the `kibana/values-production.yaml` file appropriately and then run:

```$xslt
helm -n logging install kibana kibana -f kibana/values-production.yaml 
```

### Postgresql-ha

To `install` postgresql-ha with `helm`, update the `postgresql-ha/values-production.yaml` file appropriately and then run:

```$xslt
helm install postgresql postgresql-ha -f postgresql-ha/values-production.yaml 
```

