
general = {
  resource_group_name   = "My-Resource-Group"
  location              = "canadacentral"
}

cdn_profile = {
  name  = "prj-cdn"
  sku   = "Standard_Akamai"
}

cdn_endpoint = {
  name          = "prj-cdn"
}

cdn_endpoint_origin = {
  host_name = "web.devprimer.com"
}




