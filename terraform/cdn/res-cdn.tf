
resource "azurerm_cdn_profile" "domain" {
  name                = var.cdn_profile.name
  location            = var.general.location
  resource_group_name = data.azurerm_resource_group.project.name
  sku                 = var.cdn_profile.sku    # e.g. "Standard_Akamai"

}
resource "azurerm_cdn_endpoint" "web" {
  name                = var.cdn_endpoint.name
  profile_name        = azurerm_cdn_profile.domain.name
  location            = var.general.location
  resource_group_name = data.azurerm_resource_group.project.name

  origin {
    name      = var.cdn_endpoint.name
    host_name = var.cdn_endpoint_origin.host_name
  }

  origin_host_header = var.cdn_endpoint_origin.host_name
}