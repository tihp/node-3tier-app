variable "subscription_id" {}
variable "client_id" {}
variable "client_secret" {}
variable "tenant_id" {}

variable "general" {
  type = object({
    resource_group_name = string
    location            = string
  })
}

variable "cdn_profile" {
  type = object({
    name = string
    sku  = string
  })
}

variable "cdn_endpoint" {
  type = object({
    name = string
  })
}

variable "cdn_endpoint_origin" {
  type = object({
    host_name = string
  })
}
