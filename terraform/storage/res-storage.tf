
resource "azurerm_storage_account" "project" {
  name                     = "${var.storage_account.name}${random_id.keyvault.hex}"
  resource_group_name      = data.azurerm_resource_group.project.name
  location                 = data.azurerm_resource_group.project.location
  account_kind             = "StorageV2"
  account_tier             = "Standard"
  account_replication_type = "RAGRS"

  lifecycle {
    ignore_changes = [
      tags,
    ]
  }
}

resource "random_id" "keyvault" {
  byte_length = var.storage_account.post_id_len  # e.g. 4
}

resource "azurerm_storage_container" "tfstate" {
  name                  = var.storage_container.name                  # e.g. tfstate
  storage_account_name  = azurerm_storage_account.project.name
  container_access_type = var.storage_container.container_access_type # "private"
}

