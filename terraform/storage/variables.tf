variable "subscription_id" {}
variable "client_id" {}
variable "client_secret" {}
variable "tenant_id" {}

variable "general" {
    type = object({
      resource_group_name = string
      location            = string
    })
}

variable "storage_account" {
  type = object({
    name        = string
    post_id_len = number
  })
}

variable "storage_container" {
  type = object({
    name                  = string
    container_access_type = string
  })
}
