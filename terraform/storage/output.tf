
output "storageaccount-name" {
  value = azurerm_storage_account.project.name
}

output "storageaccount-primary_key" {
  value = azurerm_storage_account.project.primary_access_key
}