
general = {
  resource_group_name   = "My-Resource-Group"
  location              = "canadacentral"
}

storage_account = {
  name        = "prjstg"
  post_id_len = 4
}

storage_container = {
  name                  = "tfstate"
  container_access_type = "private"
}

