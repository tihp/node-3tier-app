# Terraform Storage Codes

## Introduction
This repo contains the Terraform codes to create an Azure storage account and container to store the Terraform state 
files. Storing state files on the cloud is a recommended practice as it allows collaborations with the team members 
(locally stored state files by default won't give you this capability) 

1. Modify the `init.sh` file with the service provider information.

2. Update the `prod.tfvars` file appropriately.

3. Source the `init.sh` file by running `source init.sh` on bash

4. Create a new workspace 
    ```$xslt
    terraform workspace new prod
    ```
5. Initialize make.

    ```$xslt
    make init
    ```

6. Run plan and apply.

    ```$xslt
    make plan
    make apply
    ```

7. To get the storage account name and key, run:

    ```$xslt
    terraform show
    ```