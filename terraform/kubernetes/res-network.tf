
resource "azurerm_virtual_network" "k8s" {
  name                = "${var.k8s_general.cluster_name}-vnet"
  resource_group_name = data.azurerm_resource_group.project.name
  location            = data.azurerm_resource_group.project.location
  address_space       = var.k8s_virtual_network.address_space

  lifecycle {
    ignore_changes = [
      tags,
    ]
  }
}

resource "azurerm_subnet" "k8s" {
  name                 = "${var.k8s_general.cluster_name}-subnet"
  resource_group_name  = var.general.resource_group_name
  virtual_network_name = azurerm_virtual_network.k8s.name
  address_prefix       = var.k8s_subnet.address_prefix
}
