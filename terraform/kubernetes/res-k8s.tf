
resource "azurerm_kubernetes_cluster" "k8s" {
  name                  = "${var.k8s_general.cluster_name}-aks"
  resource_group_name   = var.general.resource_group_name
  location              = var.general.location
  dns_prefix            = "${var.k8s_general.cluster_name}-dns"
  kubernetes_version    = var.k8s_general.aks_version
  private_link_enabled  = var.k8s_general.private_link_enabled

  linux_profile {
    admin_username = var.k8s_general.admin_username

    ssh_key {
      key_data = file(var.k8s_general.ssh_key)
    }
  }

  default_node_pool {
    name            = var.k8s_node.name
    node_count      = var.k8s_node.node_count
    vm_size         = var.k8s_node.vm_size
    os_disk_size_gb = var.k8s_node.os_disk_size_gb
    type            = var.k8s_node.type
    # Required for advanced networking
    vnet_subnet_id = azurerm_subnet.k8s.id
  }

  network_profile {
    dns_service_ip     = var.k8s_network.dns_service_ip     # "10.100.100.100"
    docker_bridge_cidr = var.k8s_network.docker_bridge_cidr # "172.17.0.1/16"
    load_balancer_sku  = var.k8s_network.load_balancer_sku  # "Standard" or "Basic"
    network_plugin     = var.k8s_network.network_plugin     # "kubenet" or "azure"
    pod_cidr           = var.k8s_network.pod_cidr           # "10.200.0.0/16"
    service_cidr       = var.k8s_network.service_cidr       # "10.100.0.0/16"
  }

  role_based_access_control {
    enabled = var.k8s_node.role_based_access_control
  }

  service_principal {
    client_id     = var.client_id
    client_secret = var.client_secret
  }

  addon_profile {
    kube_dashboard {
      enabled   = var.k8s_node.kube_dashboard
    }
  }

  lifecycle {
    ignore_changes = [
      tags,
    ]
  }
}

