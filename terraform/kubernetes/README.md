# Terraform Kubernetes Codes

## Introduction
This repo contains the Terraform codes to create an Azure Kubernetes Service resource.

1. Update the `init.sh` file appropriately. 
Update `ARM_ACCESS_KEY` with the storage account key (value from `storageaccount-primary_key`).
Update `storage_account_name` (value from `storageaccount-name`).

2. Update the `prod.tfvars` file appropriately.

3. Source the `init.sh` file by running `source init.sh` on bash

4. Create a new workspace 
    ```$xslt
    terraform workspace new prod
    ```
   
5. Run plan and apply.

    ```$xslt
    make plan
    make apply
    ```
After a successful run, the k8s configure file `kube.config-k8s` will get created in the `./output` directory.