variable "subscription_id" {}
variable "client_id" {}
variable "client_secret" {}
variable "tenant_id" {}

variable "general" {
  type = object({
    resource_group_name = string
    location              = string
  })
}

variable "k8s_general" {
  type = object({
    cluster_name          = string   # Azure Kubernetes cluster name, eg: aks-rancher
    aks_version           = string   # Kubernetes version
    private_link_enabled  = bool     # Place the control plane in the same subnet as the nodes
    admin_username        = string
    ssh_key               = string
    kubeconfig_path       = string
  })
}

variable "k8s_node" {
  type = object({
    name                      = string
    node_count                = number
    vm_size                   = string
    os_disk_size_gb           = number
    type                      = string
    role_based_access_control = bool
    kube_dashboard            = bool

  })
}

variable "k8s_network" {
  type = object({
    dns_service_ip      = string
    docker_bridge_cidr  = string
    load_balancer_sku   = string
    network_plugin      = string
    pod_cidr            = string
    service_cidr        = string
  })
  default = {
    dns_service_ip     = "10.100.100.100"
    docker_bridge_cidr = "172.17.0.1/16"
    load_balancer_sku  = "Standard"       # "Standard" or "Basic"
    network_plugin     = "kubenet"        # "kubenet" or "azure"
    pod_cidr           = "10.200.0.0/16"
    service_cidr       = "10.100.0.0/16"
  }
}

variable "k8s_subnet" {
  type = object({
    address_prefix  = string
  })
}

variable "k8s_virtual_network" {
  type = object({
    address_space   = list(string)
  })
}

