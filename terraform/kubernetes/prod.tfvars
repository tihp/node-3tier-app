
general = {
  resource_group_name   = "My-Resource-Group"
  location              = "canadacentral"
}

k8s_general = {
  cluster_name          = "prj-k8s"
  aks_version           = "1.16.7"
  private_link_enabled  = false
  admin_username        = "azureuser"
  ssh_key               = "config/aks/id_rsa.pub"
  kubeconfig_path       = "./output/kube.config-k8s"
}

k8s_node = {
  name                      = "mainpool"
  node_count                = 2
  vm_size                   = "Standard_B4ms"
  os_disk_size_gb           = 60
  type                      = "VirtualMachineScaleSets"
  role_based_access_control = true
  kube_dashboard            = false
}

k8s_subnet = {
  address_prefix  = "10.1.0.0/16"
}

k8s_virtual_network = {
  address_space = ["10.0.0.0/8"]
}

