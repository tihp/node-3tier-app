
resource "local_file" "create_kubeconfig" {
  depends_on = [
    azurerm_kubernetes_cluster.k8s
  ]
  content  = azurerm_kubernetes_cluster.k8s.kube_config_raw
  filename = var.k8s_general.kubeconfig_path
}