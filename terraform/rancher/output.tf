
output "rancher-url" {
  value = "rancher.${var.dns_zone.name}"
}

output "rancher-username" {
  value = "admin"
}

output "rancher-password" {
  value = random_id.rancher_password.hex
}