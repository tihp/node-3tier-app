
general = {
  resource_group_name   = "My-Resource-Group"
  location              = "canadacentral"
}

dns_zone = {
  name  = "yourdomain.com"
}

k8s = {
  kubeconfig_path  = "../kubernetes/output/kube.config-k8s"
}

rancher = {
  default_password                                = "admin"
  new_password_len                                = 12
  cattle_system_namespace_manifest_path           = "config/rancher/cattle_system_namespace.yaml"
  cattle_system_tls_ca_manifest_path              = "config/rancher/yourdomain.com/cattle_system_tls_ca.yaml"
  cattle_system_tls_rancher_ingress_manifest_path = "config/rancher/yourdomain.com/cattle_system_tls_rancher_ingress.yaml"
  values_path                                     = "config/rancher/yourdomain.com/rancher_values.yaml"
}


