provider "azurerm" {
  version = "~>2.3"

  subscription_id = var.subscription_id
  client_id       = var.client_id
  client_secret   = var.client_secret
  tenant_id       = var.tenant_id

  features {}
}

provider "null" {
  version = "~>2.1"
}

provider "helm" {
  version = "~>1.1"
  kubernetes {
    config_path = var.k8s.kubeconfig_path
  }
}

provider "k8s" {
  version = "~>0.7"
  config_path = var.k8s.kubeconfig_path
}

provider "local" {
  version = "~>1.4"
}

provider "kubernetes" {
  version = "~>1.11"
  config_path = var.k8s.kubeconfig_path

}

provider "random" {
  version = "~>2.2"
}