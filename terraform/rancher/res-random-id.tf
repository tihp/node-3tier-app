
resource "random_id" "rancher_password" {
  byte_length = var.rancher.new_password_len
}
