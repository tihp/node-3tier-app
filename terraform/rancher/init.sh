#!/bin/bash
export ENVIRONMENT=prod

export TF_VAR_subscription_id=
export TF_VAR_client_id=
export TF_VAR_client_secret=
export TF_VAR_tenant_id=

export BC_RGN=resource_group_name=My-Resource-Group
export BC_CTN=container_name=tfstate
export BC_STG=storage_account_name=prjstgXXXXXX
export BC_KEY=key=project.rancher
export ARM_ACCESS_KEY=XXXXXXXXXXXXXXXXXX

terraform init -var-file="$ENVIRONMENT".tfvar \
  -backend-config "$BC_RGN" \
  -backend-config "$BC_CTN" \
  -backend-config "$BC_STG" \
  -backend-config "$BC_KEY"

#terraform workspace new "$ENVIRONMENT"
