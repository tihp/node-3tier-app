
locals {
  rancher_domain       = "rancher.${var.dns_zone.name}"

  logintoken_url       = "https://${local.rancher_domain}/v3-public/localProviders/local?action=login"
  changepassword_url   = "https://${local.rancher_domain}/v3/users?action=changepassword"
  seturl_url           = "https://${local.rancher_domain}/v3/settings/server-url"
  telemetryoptout_url  = "https://${local.rancher_domain}/v3/settings/telemetry-opt"
  apitoken_url         = "https://${local.rancher_domain}/v3/token"

  content_type = "content-type: application/json"

  login_payload = {
    username : "admin"
    password : var.rancher.default_password
    ttl: 600000
    }

  changepassword_payload = {
    currentPassword : var.rancher.default_password
    newPassword     : random_id.rancher_password.hex
  }

  seturl_payload = {
    name  : "server-url"
    value : "https://${local.rancher_domain}"
  }

  telemetryoptout_payload = {
    value: "out"
  }

  apitoken_payload = {
    type: "token"
    description: "Requested from Terraform"
  }
}

/* Login token good for 1 minute
curl -k -s 'https://aks-rancher.devprimer.com/v3-public/localProviders/local?action=login' \
  -H 'content-type: application/json' \
  --data-binary '{"username":"admin","password":"admin","ttl":60000}' | jq -r .token
*/

module "rancher_logintoken" {
  source  = "./modules/shell-resource"

  depends = [
    helm_release.rancher
  ]

//  trigger = timestamp()
  command = <<EOT
curl -k -s -f ${local.logintoken_url} \
-H '${local.content_type}' \
--data-binary '${jsonencode(local.login_payload)}'
EOT
}

//output "rancher_login" {
//  value =  jsondecode(module.rancher_logintoken.stdout).token
//}

/* Create permanent API Token
curl -k -s 'https://aks-rancher.devprimer.com/v3/token' \
  -H 'Content-Type: application/json' -H "Authorization: Bearer $APIKEY" \
  -X PUT --data-binary '{"type":"token", "description":"Requested from Terraform"}'
*/
module "rancher_apitoken" {
  source  = "./modules/shell-resource"

  depends = [
    module.rancher_logintoken.id
  ]

  //  trigger = timestamp()
  command = <<EOT
curl -k -s -f ${local.apitoken_url} \
-H '${local.content_type}' \
-H 'Authorization: Bearer ${jsondecode(module.rancher_logintoken.stdout).token}' \
--data-binary '${jsonencode(local.apitoken_payload)}'
EOT
}

resource "local_file" "rancher_apitoken" {
  depends_on = [
    module.rancher_apitoken.id
  ]
  content     = jsondecode(module.rancher_apitoken.stdout).token
  filename = "${path.module}/out/rancher_apitoken"
}

//output "rancher_apitoken" {
//  value = jsondecode(module.rancher_apitoken.stdout).token
//}

/* Delete temporary login access token
curl -k -s 'https://aks-rancher.devprimer.com/v3/token/<token-name>' \
  -H 'Content-Type: application/json' -H "Authorization: Bearer $APIKEY" \
  -X DELETE'
*/
module "rancher_deletelogintoken" {
  source  = "./modules/shell-resource"

  depends = [
    module.rancher_apitoken.id
  ]

  //  trigger = timestamp()
  command = <<EOT
curl -k -s -f '${local.apitoken_url}/${jsondecode(module.rancher_logintoken.stdout).name}' \
-H '${local.content_type}' \
-H 'Authorization: Bearer ${jsondecode(module.rancher_apitoken.stdout).token}' \
-X DELETE
EOT
}

//output "rancher_deletelogintoken" {
//  value = module.rancher_deletelogintoken
//}

/* Change password
curl -k -s 'https://aks-rancher.devprimer.com/v3/users?action=changepassword' \
  -H 'Content-Type: application/json' -H "Authorization: Bearer $LOGINTOKEN" \
  --data-binary '{"currentPassword":"admin","newPassword":"admin123"}'
*/
module "rancher_changepassword" {
  source  = "./modules/shell-resource"

  depends = [
    module.rancher_apitoken.id
  ]

//  trigger = timestamp()
  command = <<EOT
curl -k -s -f ${local.changepassword_url} \
-H '${local.content_type}' \
-H 'Authorization: Bearer ${jsondecode(module.rancher_apitoken.stdout).token}' \
--data-binary '${jsonencode(local.changepassword_payload)}'
EOT
}

//output "rancher_changepassword" {
//  value = module.rancher_changepassword
//}

/* Set server-url
curl -k -s 'https://aks-rancher.devprimer.com/v3/settings/server-url' \
  -H 'Content-Type: application/json' -H "Authorization: Bearer $APIKEY" \
  -X PUT --data-binary '{"name":"server-url","value":"https://aks-rancher.devprimer.com"}'
*/
module "rancher_seturl" {
  source  = "./modules/shell-resource"

  depends = [
    module.rancher_changepassword.id
  ]

  //  trigger = timestamp()
  command = <<EOT
curl -k -s -f ${local.seturl_url} \
-H '${local.content_type}' \
-H 'Authorization: Bearer ${jsondecode(module.rancher_apitoken.stdout).token}' \
-X PUT \
--data-binary '${jsonencode(local.seturl_payload)}'
EOT
}

//output "rancher_seturl" {
//  value = module.rancher_seturl
//}

/* Opt-out telemetry
curl -k -s 'https://aks-rancher.devprimer.com/v3/settings/telemetry-opt' \
  -H 'Content-Type: application/json' -H "Authorization: Bearer $APIKEY" \
  -X PUT --data-binary '{"value":"out"}'
*/
module "rancher_telemetryoptout" {
  source  = "./modules/shell-resource"

  depends = [
    module.rancher_seturl.id
  ]

  //  trigger = timestamp()
  command = <<EOT
curl -k -s -f ${local.telemetryoptout_url} \
-H '${local.content_type}' \
-H 'Authorization: Bearer ${jsondecode(module.rancher_apitoken.stdout).token}' \
-X PUT \
--data-binary '${jsonencode(local.telemetryoptout_payload)}'
EOT
}

//output "rancher_telemetryoptout" {
//  value = module.rancher_telemetryoptout
//}




