
resource "azurerm_dns_zone" "domain" {
  name                = var.dns_zone.name
  resource_group_name = data.azurerm_resource_group.project.name

  lifecycle {
    ignore_changes = [
      tags,
    ]
  }
}

resource "azurerm_dns_a_record" "root" {
  depends_on = [
    azurerm_dns_zone.domain,
  ]

  name                = "@"
  zone_name           = var.dns_zone.name
  resource_group_name = var.general.resource_group_name
  ttl                 = 300
  records             = [data.kubernetes_service.nginx_ingress_controller.load_balancer_ingress.0.ip]
}

resource "azurerm_dns_a_record" "rancher" {
  depends_on = [
    azurerm_dns_zone.domain,
  ]

  name                = "rancher"
  zone_name           = var.dns_zone.name
  resource_group_name = var.general.resource_group_name
  ttl                 = 300
  records             = [data.kubernetes_service.nginx_ingress_controller.load_balancer_ingress.0.ip]
}

resource "azurerm_dns_a_record" "api" {
  depends_on = [
    azurerm_dns_zone.domain,
  ]

  name                = "api"
  zone_name           = var.dns_zone.name
  resource_group_name = var.general.resource_group_name
  ttl                 = 300
  records             = [data.kubernetes_service.nginx_ingress_controller.load_balancer_ingress.0.ip]
}

resource "azurerm_dns_a_record" "web" {
  depends_on = [
    azurerm_dns_zone.domain,
  ]

  name                = "web"
  zone_name           = var.dns_zone.name
  resource_group_name = var.general.resource_group_name
  ttl                 = 300
  records             = [data.kubernetes_service.nginx_ingress_controller.load_balancer_ingress.0.ip]
}


resource "azurerm_dns_a_record" "nginx_ingress" {
  depends_on = [
    azurerm_dns_zone.domain,
  ]

  name                = "*"
  zone_name           = var.dns_zone.name
  resource_group_name = var.general.resource_group_name
  ttl                 = 300
  records             = [data.kubernetes_service.nginx_ingress_controller.load_balancer_ingress.0.ip]
}

