
data "kubernetes_service" "nginx_ingress_controller" {
  metadata {
    name = "nginx-ingress-controller"
    namespace = "kube-system"
  }
}