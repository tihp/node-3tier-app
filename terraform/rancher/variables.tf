variable "subscription_id" {}
variable "client_id" {}
variable "client_secret" {}
variable "tenant_id" {}

variable "general" {
  type = object({
    resource_group_name = string
    location            = string
  })
}

variable "dns_zone" {
  type = object({
    name = string
  })
}

variable "k8s" {
  type = object({
    kubeconfig_path = string
  })
}

variable "rancher" {
  type = object({
    default_password                                = string
    new_password_len                                = number
    cattle_system_namespace_manifest_path           = string
    cattle_system_tls_ca_manifest_path              = string
    cattle_system_tls_rancher_ingress_manifest_path = string
    values_path                                     = string
  })
}


