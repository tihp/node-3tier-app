# Terraform Storage Codes

## Introduction
This repo contains the Terraform codes to install the Rancher component in the Kubernetes cluster. 
These codes require a successful run of the ```kubernetes``` tf codes as a working `kube.config-k8s` file is needed.

## Requirements

Helm version 3 or newer is required ([download link](https://helm.sh/docs/intro/install/)).

Terraform `terraform-k8s-provider` is required ([download link](https://github.com/banzaicloud/terraform-provider-k8s))

## Preparing SSL certificate

This deployment expects a working SSL certificate already embedded in two manifest: `cattle_system_tls_ca.yaml` and
`cattle_system_tls_rancher_ingress.yaml`

Also remember to update the ```./config/rancher/rancher_values.yaml``` file appropriately.

### cattle_system_tls_ca.yaml

This manifest contains the signer of the SSL certificate. Export the signer cert in PEM format with the filename 
`cacerts.pem`

```$xslt
kubectl -n cattle-system create secret generic tls-ca \
   --from-file=cacerts.pem --dry-run=true \
    -o yaml > ./config/rancher/yourdomain.com/cattle_system_tls_ca.yaml
```

### cattle_system_tls_rancher_ingress.yaml

This manifest contains SSL private key and public certificate. Export these files as PEM format with the filenames 
`tls.key` and `tls.crt`.

```$xslt
kubectl -n cattle-system create secret tls tls-rancher-ingress \
    --cert=tls.crt --key=tls.key --dry-run=true \
    -o yaml > ./config/rancher/yourdomain.com/cattle_system_tls_rancher_ingress.yaml
```

## Instructions

1. Update the `init.sh` file appropriately. 
Update `ARM_ACCESS_KEY` with the storage account key (value from `storageaccount-primary_key`).
Update `storage_account_name` (value from `storageaccount-name`).

2. Update the `prod.tfvars` file appropriately.

3. Source the `init.sh` file by running `source init.sh` on bash

4. Create a new workspace 
    ```$xslt
    terraform workspace new prod
    ```
   
5. Run plan and apply.

    ```$xslt
    make plan
    make apply
    ```
   
