
resource "k8s_manifest" "cattle_system_namespace" {
  content = file(var.rancher.cattle_system_namespace_manifest_path)
}

resource "k8s_manifest" "cattle_system_tls_ca" {
  depends_on = [
    k8s_manifest.cattle_system_namespace,
  ]
  content = file(var.rancher.cattle_system_tls_ca_manifest_path)
}

resource "k8s_manifest" "cattle_system_tls_rancher_ingress" {
  depends_on = [
    k8s_manifest.cattle_system_namespace,
  ]
  content = file(var.rancher.cattle_system_tls_rancher_ingress_manifest_path)
}

