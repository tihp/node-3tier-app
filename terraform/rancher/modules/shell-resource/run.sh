#!/usr/bin/env sh
set -eu

_path=$1; shift
_id=$1; shift

set +e
  2>"$_path/stderr.$_id" >"$_path/stdout.$_id" sh -c "$@"
  retcode=$?
  >"$_path/exitstatus.$_id" echo $retcode
  exit $retcode
set -e
