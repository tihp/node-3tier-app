
resource "null_resource" "rancher_helm_repo" {
  provisioner "local-exec" {
    command = "helm repo add rancher-latest https://releases.rancher.com/server-charts/latest"
  }
  provisioner "local-exec" {
    command = "helm repo update"
  }
}

resource "helm_release" "rancher" {
  depends_on = [
    null_resource.rancher_helm_repo,
    k8s_manifest.cattle_system_namespace,
    k8s_manifest.cattle_system_tls_ca,
    k8s_manifest.cattle_system_tls_rancher_ingress,
  ]
  name      = "rancher"
  chart     = "rancher-latest/rancher"
  namespace = "cattle-system"
  values = [
    file(var.rancher.values_path),
  ]
}

