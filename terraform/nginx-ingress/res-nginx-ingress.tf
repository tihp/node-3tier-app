
resource "null_resource" "nginx_ingress_helm_repo" {
  provisioner "local-exec" {
    command = "helm repo update"
  }
}

resource "helm_release" "nginx_ingress" {
  depends_on = [
    null_resource.nginx_ingress_helm_repo
  ]
  name      = "nginx-ingress"
  chart     = "stable/nginx-ingress"
  namespace = "kube-system"
  version   = var.nginx_ingress.version

  set {
    name  = "controller.replicaCount"
    value = var.nginx_ingress.replica_count
  }

  set {
    name  = "controller.nodeSelector.beta\\.kubernetes\\.io/os"
    value = "linux"
  }

  set {
    name  = "defaultBackend.nodeSelector.beta\\.kubernetes\\.io/os"
    value = "linux"
  }
}