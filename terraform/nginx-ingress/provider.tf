
provider "null" {
  version = "~>2.1"
}

provider "helm" {
  version = "~>1.1"
  kubernetes {
    config_path = var.k8s.kubeconfig_path
  }
}

