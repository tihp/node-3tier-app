
variable "k8s" {
  type = object({
    kubeconfig_path = string
  })
}

variable "nginx_ingress" {
  type = object({
    version       = string
    replica_count = number
  })
}

