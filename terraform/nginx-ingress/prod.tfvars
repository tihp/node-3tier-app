
k8s = {
  kubeconfig_path  = "../kubernetes/output/kube.config-k8s"
}

nginx_ingress = {
  version       = "1.36.0"
  replica_count = "2"
}

