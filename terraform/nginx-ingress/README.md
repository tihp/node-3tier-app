# Terraform nginx-ingress Codes

## Introduction
This repo contains the Terraform codes to install the `nginx-ingress` component in the Kubernetes cluster. 
These codes require a successful run of the ```kubernetes``` tf codes as a working `kube.config-k8s` file is needed.

## Requirements

Helm version 3 or newer is required ([helm installation link](https://helm.sh/docs/intro/install/)).

## Instructions

1. Update the `init.sh` file appropriately. 
Update `ARM_ACCESS_KEY` with the storage account key (value from `storageaccount-primary_key`).
Update `storage_account_name` (value from `storageaccount-name`).

2. Update the `prod.tfvars` file appropriately.

3. Source the `init.sh` file by running `source init.sh` on bash

4. Create a new workspace 
    ```$xslt
    terraform workspace new prod
    ```
   
5. Run plan and apply.

    ```$xslt
    make plan
    make apply
    ```