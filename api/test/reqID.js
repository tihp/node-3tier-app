
var expect = require('chai').expect;
var reqID = require('../reqID');

describe('reqID()', function () {
    it('should produce UUID version 4 string with a length of 36 characters', function () {
        var id = reqID();
        expect(id).to.be.a('string').lengthOf(36);
    });
});