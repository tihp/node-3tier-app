# Devops API App

## Manual Deployment

Install the node packages for the api tier:

```$xslt
npm install
```

To test the app:
```$xslt
npm test
```

Start the app:

```$xslt
npm start
```

- PORT: the listening PORT
- DB: the postgresql url to connect

These two variables need to be set

## Rancher's CICD Pipeline Deployment

This applications can be deployed with Rancher's CICD Pipelines feature 
([link](https://rancher.com/docs/rancher/v2.x/en/k8s-in-rancher/pipelines/)).


### Requirements

A running postgres database instance is required. 

This k8s deployment expects a secret with the name `db` and a key name `DB` to pre-exist with the `db` URL  value

Below is an example of a `DB` key value:

```$xslt
postgres://<username>:<password>@<postgres-host>/<db-instance>	
``` 

For example, to create the `db` secret with `kubectl`.

```$xslt
kubectl create secret generic db --from-literal=DB=postgres://postgres:mypassword@postgresql-postgresql-ha-pgpool/postgres	
```


